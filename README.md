# 概述

Bilingual Research Helper (BRH)是在做科技部评审专家智能指派项目中写过的一个工具，提供跨语言研究的几个常用功能。

NLP跨语言研究常有的需求有两个：跨语言链接的获取，跨语言语料的训练。

* 跨语言语料的获取
    * DBpedia
    * Translation
* 跨语言语料的训练
    * Mapping
    * Joint Embedding
    
BRH中包含了除Mapping以外的其他三个功能。

### 依赖的包

* [hanziconv](https://github.com/berniey/hanziconv)
* [py-googletrans](https://github.com/ssut/py-googletrans)
* gensim 3.0+
* sklearn

### 文档链接

[网页版](https://lvmememe.gitlab.io/BRH)/[PDF版](https://lvmememe.gitlab.io/BRH/book.pdf)