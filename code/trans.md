# 谷歌翻译

```python
class Trans:
    '''
    运行程序的机器需要能够访问谷歌翻译，ipv6能访问也可以
    '''
    def __init__(self, fn, src = 'zh-cn', dest = 'en'):
        '''
        传入待翻译的文件名、源语言、目标语言（关于更多语言代码可以参考http://py-googletrans.readthedocs.io/en/latest/#googletrans-languages）
        '''
        
    def translate(self, lpt = 200):
        '''
        每次合并lpt行，减少HTTP请求次数，一块请求回来再拆开
        '''
```