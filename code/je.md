# Joint Embedding

通过关键词连接的双语语料通常有两种训练方法：

* Mapping
    
    假定有一个矩阵M可以实现两个语料空间的线性变换，所以就单独训练两种语言，然后再训练一个矩阵M。通常在两个语料库平行明显的时候效果还可以。
    
* Joint Embedding

    让具有跨语言关系的词强制坐标相等，相当于把两个词合并了。利用这些合并的词作为两种语言的语料库合并，训练一个模型。
    

```python
class JE:
    @staticmethod
    def buildvocab(fns):
        '''
        给定语料，返回词典
        '''
    
    def __init__(self, en_corpus, zh_corpus, ILL, fnout = 'word2id.pkl', needTrain = True):
        '''
        en_corpus和zh_corpus都是文件名列表，每个文件有若干行语料，每行都是用\t分隔的若干个关键词
        ILL是英中跨语言链接文件名，每行两个关键词，用\t分隔，第一个词是en_corpus的词，第二个是zh_corpus的词
        '''
    
    def word2id(self, fnout):
        '''
        核心思想就是把每个词映射到一个id，具有跨语言关系的词会被映射到同一个id
        '''
    
    def train(self, saveat = 'keyword_model'):
        '''
        用CBOW训练word2vec模型并保存
        '''

    def query(self, word):
        '''
        查询词向量
        '''
```